public class SeatBooking{
	public static void main(String args[]){
		int num=9;
		System.out.println("| SEAT NUMBER | "+num+" |");
		int i=1;
		int n=0;
		while(n<=num){
			n=4*i-3;
			i=i+1;
			if(n%num==0){
				System.out.println("-THIS IS A LEFT SIDE WINDOW SEAT-");
				break;
			}else if(n>num){
				System.out.println("-THIS IS NOT A WINDOW SEAT-|");
				break;
			}
		if(num%4==0){
				System.out.println("-THIS IS A RIGHT SIDE WINDOW SEAT-");
				break;
		}
		}
	}
}
/*
| SEAT NUMBER | 10 |
-THIS IS NOT A WINDOW SEAT-|

| SEAT NUMBER | 9 |
-THIS IS A LEFT SIDE WINDOW SEAT-

| SEAT NUMBER | 4 |
-THIS IS A RIGHT SIDE WINDOW SEAT-
*/

		/*
		System.out.println(" _____         ______ ");
		System.out.println("|_LEFT         RIGHT_|");
		System.out.println("");
		System.out.println("|  1    2   3     4  |");
		System.out.println("|  5    6   7     8  |");
		System.out.println("|  9    10  11    12 |");
		System.out.println("|  13   14  15    16 |");
		System.out.println("   |    |   |     |  ");
		System.out.println("   |    |   |     |  ");
		*/