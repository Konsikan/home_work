public class LeapYear{
	public static void main(String args[]){
		int year=2020;
		System.out.println("| "+year+" |");
		if (year%100!=0 && (year%400==0 | year%4==0)){
			System.out.println("| THIS IS A LEAP YEAR |");
		}else{
			System.out.println("| THIS IS NOT A LEAP YEAR |");
		}
	}
}
/*

OUTPUT

| 2020 |
| THIS IS A LEAP YEAR |


*/