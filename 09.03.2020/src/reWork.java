import java.util.Scanner;

public class reWork {
	public void fGrade(int marks1) {
		Scanner scan = new Scanner(System.in);
		System.out.print("ENTER YOUR MARKS : ");
		int marks = scan.nextInt();
		System.out.println("");
		if ((marks >= 0) && (marks <= 100)) {
			if (marks >= 75) {
				System.out.println("GRADE-|A|");
			} else if (marks >= 60) {
				System.out.println("GRADE-|B|");
			} else if (marks >= 45) {
				System.out.println("GRADE-|C|");
			} else {
				System.out.println("GRADE-|D|");
			}
		} else {
			System.out.println("INVALID MARKS");
		}
	}
	public void unit(int unit1){
		System.out.println("");
		Scanner scan = new Scanner(System.in);
		System.out.print("ENTER YOUR UNIT : ");
		int unit = scan.nextInt();
		System.out.println("");
		System.out.println("   CONSUMPTION     COST PER UNIT");
		System.out.println("|0-30      UNITS |  Rs  3.00");
		System.out.println("|31-60     UNITS |  Rs  5.00");
		System.out.println("|61-90     UNITS |  Rs 10.00");
		System.out.println("|91-120    UNITS |  Rs 20.00");
		System.out.println("|121-180   UNITS |  Rs 30.00");
		System.out.println("|181-210   UNITS |  Rs 35.00");
		System.out.println("|211-300   UNITS |  Rs 40.00");
		System.out.println("|ABOVE 300 UNITS |  Rs 50.00");
		System.out.println("");
		if(unit>=0){
			if (unit<=30){
				int cost=(unit*3);
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=31)&&(unit<=60)){
				int cost=(30*3)+(unit-30)*5;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=61)&&(unit<=90)){
				int cost=(30*3)+(30*5)+(unit-60)*10;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=91)&&(unit<=120)){
				int cost=(30*3)+(30*5)+(30*10)+(unit-90)*20;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=121)&&(unit<=180)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(unit-120)*30;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=181)&&(unit<=210)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(unit-180)*35;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else if ((unit>=211)&&(unit<=300)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(unit-210)*40;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}else{
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(90*40)+(unit-300)*50;
				System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
			}
		}else {
			System.out.println("ERROR");
		}	
		
	}
}
