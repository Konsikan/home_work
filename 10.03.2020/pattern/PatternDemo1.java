package pattern;

public class PatternDemo1 {
	public static void main(String args[]) {
		Pattern demo = new Pattern();
		int x = 7;
		demo.p1(x);
		demo.p2(x);
		demo.p3(x);
		demo.p4(x);
		demo.p5(x);
		demo.p6(x);
		demo.p7(x);
		demo.p8(x);
		demo.p12(x);
		demo.p14(x);
	}

}
