package pattern;

public class Pattern {
	// PATTERN 1

	public void p1(int num) {
		System.out.println("PATTERN 1 ");
		System.out.println("");
		int n = 1;
		do {
			for (int i = 1; i <= n; i++) {
				System.out.print(i + " ");
			}
			System.out.println("");
			n = n + 1;
		} while (n <= num);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 2
	public void p2(int num) {
		System.out.println("PATTERN 2 ");
		System.out.println("");
		int i = 1;
		do {
			for (int n = num; n > i; n--) {
				System.out.print("1");
			}
			for (int m = 1; m <= i; m++) {
				System.out.print(i);
			}
			System.out.println("");

			i = i + 1;
		} while (i <= num);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 3
	public void p3(int num) {
		System.out.println("PATTERN 3 ");
		System.out.println("");
		int n = 1;
		do {
			for (int i = 1; i <= num; i++) {
				if (i <= n) {
					System.out.print(i + " ");
				}
			}
			System.out.println("");
			n = n + 1;
		} while (n <= num);
		do {
			for (int a = 1; a <= n; a++) {
				System.out.print(a + " ");
			}
			System.out.println("");
			n = n - 1;
		} while (n > 0);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 4
	public void p4(int num) {
		System.out.println("PATTERN 4 ");
		System.out.println("");
		int n = num;
		do {
			for (int a = 1; a <= n; a++) {
				System.out.print(a + " ");
			}
			System.out.println("");
			n = n - 1;
		} while (n > 1);

		do {
			for (int i = 1; i <= num; i++) {
				if (i <= n) {
					System.out.print(i + " ");
				}
			}
			System.out.println("");
			n = n + 1;
		} while (n <= num);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 5
	public void p5(int num) {
		System.out.println("PATTERN 5 ");
		System.out.println("");
		int i = 1;
		do {
			for (int n = num; n >= i; n--) {
				System.out.print(n + " ");
			}
			System.out.println("");
			i = i + 1;
		} while (i <= num);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 6
	public void p6(int num) {
		System.out.println("PATTERN 6 ");
		System.out.println("");
		int i = num;
		do {
			for (int n = num; n >= i; n--) {
				System.out.print(n + " ");
			}
			System.out.println("");
			i = i - 1;
		} while (i > 0);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 7
	public void p7(int num) {
		System.out.println("PATTERN 7 ");
		System.out.println("");
		int n = num;
		do {
			for (int a = 1; a <= n; a++) {
				System.out.print(a + " ");
			}
			System.out.println("");
			n = n - 1;
		} while (n > 0);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 8
	public void p8(int num) {
		System.out.println("PATTERN 8 ");
		System.out.println("");
		int i = 1;
		do {
			for (int n = 1; n <= i; n++) {
				System.out.print(n + " ");
			}
			for (int m = i - 1; m >= 1; m--) {
				System.out.print(m + " ");
			}
			System.out.println();
			i = i + 1;
		} while (i <= num);
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 12
	public void p12(int num) {
		System.out.println("PATTERN 12 ");
		System.out.println("");
		int n = 1;
		double x = Math.pow(num, 2);
		for (int i = 1; i <= x; i++) {
			if (i % 2 == 0) {
				System.out.print(0);
			} else {
				System.out.print(1);
			}
			if (i % num == 0) {
				System.out.println("");
				n = n + 1;
			}
		}
		System.out.println("");
		System.out.println("");
	}

	// PATTERN 14
	public void p14(int num) {
		System.out.println("PATTERN 14 ");
		System.out.println("");
		int i = 1;
		do {
			for (int n = 1; n < i; n++) {
				System.out.print("0");
			}
			System.out.print(i);
			for (int m = i; m < num; m++) {
				System.out.print("0");
			}
			System.out.println("");
			i = i + 1;
		} while (i <= num);
	}

}
