public class ForLoopStar{
	public static void main(String args[]){
		int num=8;
		int n=1;
		double x=Math.pow(num,2);
		for(int i=1; i<=x; i++){
				if (n%2==0){
					System.out.print("#");
				}else{
					System.out.print("*");
				}
				if (i%num==0){
					System.out.println("");
					n=n+1;
				}
		}	
	}
}
/*

OUTPUT

********
########
********
########
********
########
********
########

*/